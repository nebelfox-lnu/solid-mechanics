import sympy
from sympy import sin, cos
import sympy.physics.vector as vc

n1, n2, n3, phi = sympy.symbols('η1 η2 η3 φ')
es = e1, e2, e3 = sympy.symbols('e1 e2 e3')

system = sympy.Matrix([
    2 * cos(phi) * n1 - 2 * sin(phi) * n2,
    10 * sin(phi) * n1 + 10 * cos(phi) * n2,
    sin(phi) * n1 + cos(phi) * n3
])

for xi in system:
    print('[', xi, ']')

etas = {}

for i in range(1, 4):
    system_i = [xj - int(i == j) for j, xj in enumerate(system, start=1)]
    print(f'\nSystem [i={i}]:')
    for xj in system_i:
        print('\t[', xj, ']')

    solution = sympy.solve(system_i, [n1, n2, n3])
    print(f'Solution [i={i}]:')
    for k, v in solution.items():
        key = f'{str(k)[0]}{i}{str(k)[1]}'
        print(f'\t{key}', '=', v)
        etas[key] = v

print(etas)

kontravariant = [
    sum(etas[f'η{i}{j}'] * ej for j, ej in enumerate(es, start=1))
    for i in range(1, 4)
]

print('\nKontravariant base:')
for i, Ri in enumerate(kontravariant, start=1):
    print(f'R^{i} =', Ri)

print('\nSimplified kontravariant base:')
for i, Ri in enumerate(kontravariant, start=1):
    print(f'R^{i} =', sympy.simplify(Ri))


print(f'a in e-base')
a = (3, 19, 2)
a_decart = sympy.simplify(sum(a[i] * Ri for i, Ri in enumerate(kontravariant)))
print(a_decart)
