import sympy
from sympy import sin, cos


ksi1, ksi2, ksi3, phi, a1, a2, a3 = sympy.symbols('ξ1 ξ2 ξ3 φ a1 a2 a3')
es = e1, e2, e3 = sympy.symbols('e1 e2 e3')
k = 10

xs = sympy.Matrix([
    2*ksi1 * cos(phi) + k * ksi2 * sin(phi) + ksi3 * sin(phi) + a1,
    -2*ksi1 * sin(phi) + k * ksi2 * cos(phi) + 32*a2,
    ksi3 * cos(phi) + 4*a3
])

e_values = [sympy.Matrix([int(i == j) for j in range(3)]) for i in range(3)]

print('xs:')
for i, xi in enumerate(xs):
    print(f'  * x{i}:', xi)

R = sum(xi * es[i] for i, xi in enumerate(xs))
print('R:', R)

kovariant = sympy.Matrix([
    R.diff(ksi) for ksi in (ksi1, ksi2, ksi3)
])

print('Kovariant base:')
for i, Ri in enumerate(kovariant, start=1):
    print(f'R{i}:', Ri)


gs = {}
print('G:')
for i in range(1, 4):
    for j in range(1, 4):
        key = f'g{i}{j}'
        # gij = kovariant[i-1] * kovariant[j-1]
        gij = sympy.simplify(kovariant[i-1].subs({es[i]: e_values[i] for i in range(3)}).dot(kovariant[j-1].subs({es[i]: e_values[i] for i in range(3)})))
        gs[key] = gij
        print(key, '=', gij)


print(f'a in e-base')
a = (3, 19, 2)
a_decart = sympy.simplify(sum(a[i] * Ri for i, Ri in enumerate(kovariant)))
print(a_decart)

print((2*e_values[0]*cos(phi) - 2*e_values[1]*sin(phi)).dot((2*e_values[0]*cos(phi) - 2*e_values[1]*sin(phi))))
